import { useState } from 'react';
import Buttons from './Components/Buttons/Buttons';
import SplitTime from './Components/SplitTime';
import Display from './Components/Display';

function App() {
  const [time, setTime] = useState({h:0,m:0,s:0,ms:0});
  const [interv, setInterv] = useState();

  const [lapData, setLapData] = useState([]);
  let {h,m,s,ms}=time
  // console.log(time,h,m,s,ms);
  const handleTime = ()=>{
    if(ms===100){
      s+=1
      ms=0
    }
    if(s==60){
      m+=1
      s=0
    }
    ms+=1
    return setTime({
      h:h,
      m:m,
      s:s,
      ms:ms
    })
  }
  const stopWatch = ()=>{
    clearInterval(interv)
    setStatus(2)
  }
  const startWatch = ()=>{
    setInterv(setInterval(() => {
      handleTime()
    }, 10))
    setStatus(1)
  }
  const resetWatch =()=>{
    setTime(prevState=>{
     return {h:0,m:0,s:0,ms:0}
    })
    setStatus(0)
    }
  const lap = ()=>{
    lapData.push(time)
    setLapData([...lapData])
  }


  const [status, setStatus] = useState(0);
  return (
    <div className="App">
      <Display time={time}/>
      <div className='buttons'>
      <Buttons onLap={lap} onReset={resetWatch} onStart={startWatch} onStop={stopWatch} status={status}/>
      </div>
      {lapData.length!==0&&<SplitTime data={lapData}/>}
    </div>
  );
}

export default App;
