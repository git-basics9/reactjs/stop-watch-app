import React from 'react'
import './SplitTime.css'
import SplitItem from './SplitItem';
function SplitTime({data}) {
    console.log(data);
  return (
    <>
    <div className='container'>
    {data.map((ele)=>
        <SplitItem data={ele}/>
    )}
    </div>
    </>
  )
}

export default SplitTime