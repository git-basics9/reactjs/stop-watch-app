import React from 'react'

function SplitItem({data}) {
    const {h,m,s,ms}=data
  return (
    <div className='split_box'>
    <span className='time_holder'>{h}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{m}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{s}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{ms}</span>
    </div>
  )
}

export default SplitItem