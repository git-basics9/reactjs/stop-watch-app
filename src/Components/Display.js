import React from 'react'
import './Display.css'
function Display({time}) {
  let {h,m,s,ms}=time
  if(h<10){
    h=`0${h}`
  }
  if(m<10){
    m=`0${m}`
  }
  if(s<10){
    s=`0${s}`
  }
  return (
    <>
    <div className='display_time'>
        <span className='time_holder'>{h}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{m}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{s}</span>&nbsp;:&nbsp;
        <span className='time_holder'>{ms}</span>
    </div>
    </>
  )
}

export default Display