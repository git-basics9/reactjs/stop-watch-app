import React from 'react'
import './ActionButton.css'
function ActionButton({type,action,color}) {
    // console.log("Re-Render");
  return (
    <button style={{backgroundColor:`${color}`}} className='btn' onClick={action}>{type}</button>
  )
}

export default ActionButton