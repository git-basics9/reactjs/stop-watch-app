import React from 'react'
import ActionButton from './ActionButton'

function Buttons({status,onStart,onStop,onReset,onLap,onResume}) {
  return (
    <React.Fragment>
    {status===0 && <ActionButton color='aqua' action={onStart} type='Start'/>}
    {status===1 && <><ActionButton color='aqua' action={onStop} type='Stop'/>
                     <ActionButton color='rgb(255 243 0)' action={onLap} type='Lap'/>
                   </>
    }
    {status===2 && <>
    <ActionButton color='aqua' action={onStart} type='Resume'/>
    <ActionButton color='white'action={onReset} type='Reset'/>
    <ActionButton color='rgb(255 243 0)' action={onLap} type='Lap'/>
    </>}
    </React.Fragment>
  )
}

export default Buttons